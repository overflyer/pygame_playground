import pygame
from controller import Controller
from game import Game
from character import Character


def main():
    run = True

    c = Controller()
    main_game_title = "My Pygame"

    g = Game(((c.get_screen_max_res()[0] // 3) - 10, c.get_screen_max_res()[1] - 50), main_game_title)
    colors = {"red": (255, 0, 0), "light_red": (255, 100, 100), "dark_red": (165, 0, 0), "green": (0, 255, 0),
               "light_green": (178, 255, 102), "dark_green": (0, 153, 0) , "blue": (0, 0, 255), "light_blue": (51, 150, 255),
               "dark_blue": (0, 0, 153), "purple": (127, 0, 255), "light_purple": (204, 153, 255), "dark_purple": (76, 0, 153),
               "orange": (255, 128, 0), "light_orange": (255, 178, 102), "dark_orange": (204, 110, 0), "pink": (255, 153, 153),
               "light_pink": (255, 204, 204), "dark_pink": (205, 102, 102), "yellow": (255, 255, 51), "light_yellow": (255, 255, 145),
               "dark_yellow": (255, 255, 0), "grey": (128, 128, 128), "light_grey": (224, 224, 224), "dark_grey": (96, 96, 96),
               "black": (0, 0, 0), "white": (255, 255, 255), "brown": (152, 51, 0), "turquoise": (0, 153, 153),
               "dark_turquoise": (0, 192, 102), "light_turquoise": (0, 204, 204)}

    shapes = {"rect": "rect", "circle": "circle", "polygon": "polygon", "line": "line", "lines": "line", "aaline": "aaline",
              "aalines": "aalines", "ellipse": "ellipse", "arc": "arc"}

    player = Character(g, True, False, True, True, 3, False, 100, False, 100, 100, 1, False, False, False,
                       [900, 300], (40, 60), 5, True)
    player.set_shape(shapes["rect"], (900, 300), (40, 60))
    player.set_sprite("idle.png")

    is_jumping = False
    jump_count = 10

    while run:
        c.set_framerate(30)
        g.fill_window(colors["light_blue"])
        player.draw_char_sprite(player.get_pos())
        keys = pygame.key.get_pressed()
        player_cur_pos_x = player.get_pos_x()
        player_cur_pos_y = player.get_pos_y()
        cur_screen_w = g.display_info.current_w
        cur_screen_h = g.display_info.current_h

        player.increase_velocity(0.01)

        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                run = False

        if keys[pygame.K_a]:
            player.move_x("left")
        if keys[pygame.K_d]:
            player.move_x("right")
        if not is_jumping:
            if keys[pygame.K_w]:
                player.move_y("up")
            if keys[pygame.K_s]:
                player.move_y("down")
            if keys[pygame.K_SPACE]:
                is_jumping = True
        else:
            if jump_count >= -10:
                neg = 1 # Positive factor keeps y negative so Player is moving up
                if jump_count < 0:
                    neg = -1 # Makes y positive. Character is moving back down, again
                player.set_pos_y((player.get_pos_y()) - ((jump_count ** 2) * 0.5) * neg)
                jump_count -= 1
            else:
                is_jumping = False
                jump_count = 10

        c.redraw_screen()

if __name__ == "__main__":
    main()


