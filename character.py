import pygame
from game import Game

class Character:
    def __init__(self, game_obj, is_player, is_dead, can_jump, can_shoot, lives, has_weapon, health, is_invincible, strength, defense,
                 own_level, is_boss, is_npc, is_enemy, pos, size, velocity, facing_right):
        self.player_id = 0

        if is_player and not is_npc:
            self.player_id += 1

        self.is_player = is_player
        self.game_obj = game_obj
        self.is_dead = is_dead
        self.can_jump = can_jump
        self.able_to_jump = True
        self.can_shoot = can_shoot
        self.lives = lives
        self.has_weapon = has_weapon
        self.health = health
        self.is_invincible = is_invincible
        self.strength = strength
        self.defense = defense
        self.own_level = own_level
        self.is_boss = is_boss
        self.is_npc = is_npc
        self.is_enemy = is_enemy
        self.pos = pos
        self.size = tuple(size)
        self.rect = pygame.Rect(self.pos[0], self.pos[1], self.size[0], self.size[1])
        self.velocity = velocity
        self.facing_right = facing_right
        self.sprite = None
        self.shape = None

    def move_x(self, direction):
        cur_pos_x = self.get_pos_x()
        cur_screen_w = self.game_obj.get_cur_screen_w()
        if direction == "left" and cur_pos_x >= 0:
            self.set_pos_x(cur_pos_x - self.velocity)
        elif direction == "right" and cur_pos_x <= cur_screen_w:
            self.set_pos_x(cur_pos_x + self.velocity)

        #print(self.get_pos_x())
        self.get_pos_x()

    def move_y(self, direction):
        cur_pos_y = self.get_pos_y()
        cur_screen_h = self.game_obj.get_cur_screen_h()
        if direction == "up" and cur_pos_y >= 0:
            self.set_pos_y(cur_pos_y - self.velocity)
        elif direction == "down" and cur_pos_y <= cur_screen_h:
            self.set_pos_y(cur_pos_y + self.velocity)

        #print(self.get_pos_y())
        return self.get_pos_y()

    def set_pos(self, new_pos):
        self.pos = new_pos

    def set_pos_x(self, new_pos_x):
        self.pos[0] = new_pos_x

    def set_pos_y(self, new_pos_y):
        self.pos[1] = new_pos_y

    def set_size(self, new_size):
        new_size = tuple(new_size)
        self.size = new_size

    def set_size_x(self, new_size_x):
        new_size_x = tuple(new_size_x)
        self.size = new_size_x

    def set_size_y(self, new_size_y):
        new_size_y = tuple(new_size_y)
        self.size = new_size_y

    def set_sprite(self, sprite_pth):
        self.sprite = pygame.image.load(sprite_pth)

    def set_velocity(self, new_vel):
        self.velocity = new_vel

    def set_shape(self, type_of_shape, pos, size):
        pos_x = self.get_pos_x()
        pos_y = self.get_pos_y()
        size_x = self.get_size_x()
        size_y = self.get_size_y()

        if type_of_shape == "rect":
            self.shape = pygame.Rect(pos_x, pos_y, size_x, size_y)

    def get_shape(self):
        return self.shape

    def get_pos(self):
        return self.pos

    def get_pos_x(self):
        return self.pos[0]

    def get_pos_y(self):
        return self.pos[1]

    def get_size(self):
        return self.size

    def get_size_x(self):
        return self.size[0]

    def get_size_y(self):
        return self.size[1]

    def get_velocity(self):
        return self.velocity

    def get_sprite(self):
        return self.sprite

    def draw_char_shape(self, type_of_shape, color):
        if isinstance(self.game_obj, Game):
            if type_of_shape == "rect":
                return pygame.draw.rect(self.game_obj.game_main_window, color, self.shape)
        else:
            raise TypeError("To draw shape you must pass an object of the Game class.")

    def draw_char_sprite(self, pos):
        if self.sprite:
            return self.game_obj.blit_screen(self.sprite, pos)
        else:
            raise TypeError("No sprite loaded for player, yet!")

    def decrease_velocity(self, value):
        cur_vel = self.get_velocity()
        self.set_velocity(cur_vel - value)

    def increase_velocity(self, value):
        cur_vel = self.get_velocity()
        self.set_velocity(cur_vel + value)


