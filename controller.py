import pygame


class Controller:
    def __init__(self):
        self.init_status = pygame.init()
        self.display_global_info = pygame.display.Info()
        self.wm_info = pygame.display.get_wm_info()
        self.clock = pygame.time.Clock()
        self.display = pygame.display
        self.gravity = 9.81

    def get_screen_max_h(self):
        return int(self.display_global_info.current_h)

    def get_screen_max_w(self):
        return int(self.display_global_info.current_w)

    def get_screen_max_res(self):
        max_screen_res_h = self.get_screen_max_h()
        max_screen_res_w = self.get_screen_max_w()
        max_screen_res = [max_screen_res_w, max_screen_res_h]
        return tuple(max_screen_res)

    def set_framerate(self, fr):
        self.clock.tick(fr)

    def redraw_screen(self):
        self.display.flip()
