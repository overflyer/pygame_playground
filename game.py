import pygame


class Game:
    def __init__(self, res, cap):
        self.res = res
        self.cap = cap
        self.game_main_window = pygame.display.set_mode(self.res)
        self.display_info = pygame.display.Info()
        self.game_main_title = pygame.display.set_caption(self.cap)

    def get_cur_screen_h(self):
        return int(self.display_info.current_h)

    def get_cur_screen_w(self):
        return int(self.display_info.current_w)

    def get_cur_screen_res(self):
        cur_screen_res = (int(self.get_cur_screen_w()), int(self.get_cur_screen_h()))
        return tuple(cur_screen_res)

    def fill_window(self, color):
        self.game_main_window.fill(color)

    def get_cur_window(self):
        return self.game_main_window

    def blit_screen(self, loaded_image, size):
        return self.game_main_window.blit(loaded_image, tuple(size))

    def blit_background(self, loaded_image, size):
        size = self.res
        return self.game_main_window.blit(loaded_image, size)
